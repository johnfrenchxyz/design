# Design: An Excercise in Open Source UI Creation

This is a place where I would like to manage and backup all of my UI designs, mockups, and prototypes. So much of programming and development is open source and visible, and I would like my design process to be the same.  For a standard project, I might house design files within a submodule of the main repository.  For the sake of keeping those repos clean and minimalistic, however, I will be keeping a bunch of design files I am working on here - all in one place.

For me, this simply serves as version control and backup of my design files between work and home.  But the fun part is that I want it to be public and out in the open, where even the progression of ideas and mockups is traceable.  I don’t expect this repository to be incredibly helpful to anyone except me, but I’d rather do everything I can in the open, in the off-chance that it could help even one person.

## Design Stack

I try to design as much as I can using open tools and technologies.  All of the content here has been created using [Inkscape](https://inkscape.org/en/) and is saved in a traditional SVG format. I also try to make all my designs inclusive and accessible, with as many open fonts and tools as possible.  

## Contributing

If for some reason anyone would like to contribute to these designs or propose suggestions, simply fork the project and submit a pull request.  I may not be able/inclined to translate these contributions into developed projects (and submitting a PR there would probably be best), but I welcome any potential collaboration.

If you want to contribute, I’d ask that you strive to use as many open tools, fonts, and resources as possible.  For my own benefit, I may start using this space to compile a list of these resources in the future.

## License / Usage

In the spirit of openness, please feel free to be inspired by these designs and even use elements, layouts, and color schemes in your own projects.  I would ask only for attribution within the project itself as a contributor if my work ends up being a significant part of your project in any way.  And please reach out to me directly if you do, because I’d love to know what is out there in the world!

